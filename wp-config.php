<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pennydreadfun.com');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R*zu__*jWeX|l9Yr?woW]2n;e}C%ehl4!#zt.)5QDC4SI`~Q3%Ed]fYn%X:F{)?:');
define('SECURE_AUTH_KEY',  ' KJqe/bGOrMgLIWO-W<4>AthHKM(=N? @cDw7+RY;d@)j~YJ*bW@S7M&a[?kHhKr');
define('LOGGED_IN_KEY',    'I,4}Kqp?@[BP%=&/lWJ(x/crxi4CeS;fL^Lm2xc&2Rk4/nwy3o$INSb@}CV7~Z{5');
define('NONCE_KEY',        'e8UaPW@RQ(T_Ol`N(F5=;fq7[rU~-)|.}?_-:;h0F/#>0Yv+m,&T2^H*gmtK@8F(');
define('AUTH_SALT',        ':a.SdRVOD>h2el}:=Y%S}CG<#qI8C}y_.p?6(kH*>mFkxH)umnA:I#Pd{u,zt?,K');
define('SECURE_AUTH_SALT', 'UPKb?^Pc2}y`#67ns$j`k1]2hx%rfa/ }C4i*@;fB@Gl;^^ =ZMda }>;@+V_`lK');
define('LOGGED_IN_SALT',   '>3?s$QBKOt$G`&SiG1*EIG+t39#bI-#c>UvS(3pR5p/=,^98$:Rp>_} ;6G/Zmb<');
define('NONCE_SALT',       'T-?Y/~qbTh-A>FKfU/:nzSz|o,I$mBYFVlT<ycN(?R@LcTU:vdVoXjlov=I B8rF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
