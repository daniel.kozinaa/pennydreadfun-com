var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gutil = require( 'gulp-util' );
var ftp = require( 'vinyl-ftp' );

var config = {
    php: 'pennydreadfun.com/**/*.php',
    scss: 'pennydreadfun.com/**/*.scss',
    js: 'pennydreadfun.com/src/**/*.js',
    scssin: 'pennydreadfun.com/src/scss/**/*.scss',
    scssout: 'pennydreadfun.com/assets/styles/',
    cssin: 'pennydreadfun.com/assets/styles/style.css',
    cssout: 'pennydreadfun.com/assets/styles/',
    cssoutname: 'style.css',
    jsin: 'pennydreadfun.com/src/js/**/*.js',
    jsoutname: 'scripts.js',
    jsout: 'pennydreadfun.com/assets/scripts/'
};

gulp.task('css', function() {
    return gulp.src(config.cssin)
      .pipe(concat(config.cssoutname))
      .pipe(cleanCSS())
      .pipe(gulp.dest(config.cssout));
});

gulp.task('sass', function() {
    return gulp.src(config.scssin)
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
        browsers: ['last 3 versions']
      }))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(config.scssout))
      .pipe(browserSync.stream())
});

gulp.task('js', function() {
    return gulp.src(config.jsin)
      .pipe(concat(config.jsoutname))
      .pipe(uglify())
      .pipe(gulp.dest(config.jsout));
  });

gulp.task('watch', function() {
    browserSync.init({
        proxy: 'http://localhost/pennydreadfun-com',
        host: 'localhost',
        open: 'external'
    });
    gulp.watch(config.php).on('change', browserSync.reload);
    gulp.watch(config.scss, gulp.series('sass', 'css')).on('change', browserSync.reload);
    gulp.watch(config.js, gulp.series('js')).on('change', browserSync.reload);
});


gulp.task('default', gulp.parallel('watch'));