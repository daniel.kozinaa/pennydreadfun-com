<?php wp_footer() ?>
<footer class="footer">
    <div class="gradient">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="up-text">
                        Gra została sfinansowana poprzez zbiórkę na <a href="#">Wspieram.to</a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col socials">
                    <a href="" class="social">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a href="" class="social">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a href="" class="social">
                        <i class="fab fa-instagram"></i>
                    </a>
                </div>
            </div>
            <div class="col">
                <p class="down-text">
                    <a href="#">Polityka prywatności</a>
                </p>
            </div>
        </div>
    </div>
</footer>
</body>

</html>