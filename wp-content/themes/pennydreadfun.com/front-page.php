<?php  

/* Template Name: Strona główna */ 

?>

<?php get_header(); ?>
<?php if ( have_posts() ) : ?>
<?php while ( have_posts() ) : the_post(); ?>
<section class="under-header">
    <div class="gradient">
        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/game-box.png" alt="<?php bloginfo('name'); ?>">
        <a href="#" class="default-button">Kup grę</a>
        <a href="#" class="default-button grey-button">Więcej o grze</a>
    </div>
</section>
<section class="opinion">
    <div class="header-bar">
        <h3>Sprawdź opinie o naszej grze!</h3>
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-4">
                <article class="single-opinion">
                    “Obok świetnych grafik najmocniejszym punktem Penny Dreadfun są teksty na kartach; lektura opisów
                    zdarzeń i przedmiotów powodowała u graczy wybuchy niepohamowanej wesołości.”
                </article>
            </div>
            <div class="col-md-4">
                <article class="single-opinion">
                    “Arcyciekawe podejście, multum cholernie dobrych tekstów, którym towarzyszą seraficzne
                    grafiki(...)! Penny Dreadfun to również jeden z najlepszych pomysłów na zakrapiany wieczór ze
                    znajomymi...”
                </article>
            </div>
            <div class="col-md-4">
                <article class="single-opinion">
                    “Czy polecamy ten tytuł? Jak dla nas jest to kopalnia śmiechu, która spełnia swoją funkcję – daję
                    radość i rozrywkę. Nam się bardzo podobało i chętnie siadamy do niej mimo długiej rozgrywki.”
            </div>
        </div>
    </div>
    <div class="container-fluid p-0">
        <div class="row m-0">
            <div class="col p-0 mt-5 bones-container">
                <div class="bones left"></div>
                <div class="button-container">
                    <a href="#" class="default-button pink">Kup grę</a>
                </div>
                <div class="bones right"></div>
            </div>
        </div>
    </div>
</section>
<section class="vitoria-queen padding-lessT-moreB">
    <div class="container">
        <div class="row">
            <div class="col">
                <p class="font-weight-bold">Londyn szaleje po nagłym zniknięciu królowej Wiktorii.
                    Wszyscy wiedzą, że maczały w tym palce siły nieczyste.
                    Tylko jak bardzo nieczyste były?</p>
                <p>PENNY DREADFUN: Duchy, Demony, Dickensy zabierze Was do roku 1888, do wiktoriańskiej Anglii, pełnej
                    okultyzmu, brytyjskiego humoru oraz mniej lub bardziej historycznych osób. Gracze wcielą się w
                    jedną z dziewięciu nietypowych postaci, które – kierowane różnymi pobudkami – wyruszają na
                    poszukiwania królowej Wiktorii. By odnaleźć monarchinię, bohaterowie będą musieli przeszukać
                    uliczki Londynu z czasów rewolucji industrialnej, sekretne podziemia i kanały metropolii, a w końcu
                    i dziewięć kręgów piekielnych. W czasie podróży przyjdzie im się zmierzyć z wieloma przeciwnikami,
                    zebrać liczne artefakty i towarzyszy oraz stoczyć potyczki między sobą. Zwycięzcą zostanie ten
                    bohater, który przyprowadzi Wiktorię z powrotem do Pałacu Buckingham. </p>
            </div>
        </div>
    </div>
</section>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>