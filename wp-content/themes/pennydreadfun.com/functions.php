<?php

// zastąpienie skryptów jquery 

if (!is_admin()) add_action("wp_enqueue_scripts", "register_js", 11);

function register_js() {  

	if (!is_admin()) {  

		wp_deregister_script( 'jquery' );

		wp_register_script( 'jquery', get_bloginfo('template_directory') .'/assets/scripts/jquery-3.3.1.min.js');

		wp_enqueue_script( 'jquery' );

		wp_register_script( 'bootstrap', get_bloginfo('template_directory') .'/assets/scripts/bootstrap.min.js');

		wp_enqueue_script( 'bootstrap' );

		wp_register_script( 'scripts', get_bloginfo('template_directory') .'/assets/scripts/scripts.js');

		wp_enqueue_script( 'scripts' );

	}

}

//dodawanie css'a

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function my_theme_enqueue_styles() {

	wp_enqueue_style( 'basicStyle', get_template_directory_uri() . '/style.css' );

	wp_enqueue_style( 'mainStyle', get_template_directory_uri() . '/assets/styles/style.css' );

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/styles/bootstrap.min.css' );

	wp_enqueue_style( 'fontFamily', 'https://fonts.googleapis.com/css?family=Cardo:400,700&amp;subset=latin-ext' );
	wp_enqueue_style( 'fontFamilySecond', 'https://fonts.googleapis.com/css?family=Sancreek&amp;subset=latin-ext' );
	
}

// inicjalizacja nawigacji

add_action('init', 'register_custom_menu');

function register_custom_menu() {

	register_nav_menu('main_menu', 'Menu główne');

}

// schowanie admin bar'a

show_admin_bar( false );

// wsparcie dla woocommerce
function mytheme_add_woocommerce_support() {
	add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

// wsparcie dla galerii produktow
add_action( 'after_setup_theme', 'yourtheme_setup' );
function yourtheme_setup() {
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

// remove actions
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

