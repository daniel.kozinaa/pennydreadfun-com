<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-type" content="text/html; charset=<?php bloginfo('charset'); ?>" />
	<title>
		<?php bloginfo('name'); ?>
		<?php wp_title(); ?>
	</title>
	<meta name="robots" content="index,follow" />
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP"
	 crossorigin="anonymous">
</head>

<body <?php body_class(); ?>>

	<header class="header-top">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2 col-6">
					<a href="<?php echo home_url(); ?>" class="logo">
						<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/penny-dreadfun-logo.png" alt="<?php bloginfo('name'); ?>">
					</a>
				</div>
				<div class="col-md-10 col-6 d-flex align-items-center">
					<button class="mobile-menu-btn">
						<span></span>
						<span></span>
						<span></span>
					</button>
					<nav class="menuNav">
						<ul class="menu">
							<?php wp_nav_menu( array('theme_location'=>'main_menu', 'container' => 'ul', 'items_wrap' => '%3$s')); ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>