$( document ).ready(function() {
    // dodanie klasy do header po  scrollu
    $(function() {
        var header = $(".header-top");
        var logo = $(".logo");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();
    
            if (scroll >= 10) {
                header.addClass("header-scrolled");
                logo.addClass("logo-scrolled");
            } else {
                header.removeClass("header-scrolled");
                logo.removeClass("logo-scrolled");
            }
        });
    });

    //toggle menu
    var headerTop = document.querySelector('.header-top');

    headerTop.addEventListener('click', function(){
        if(headerTop.classList.contains('active')){
            headerTop.classList.remove('active');
        }else{
            headerTop.classList.add('active');
        }
    })
});